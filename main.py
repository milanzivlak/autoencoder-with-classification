from autoencoder import Autoencoder
from cnn import ConvolutionalNetwork
from trafic_sign import TrafficSign

import torch
import torchvision
from torch import nn
import matplotlib.pyplot as plt
import numpy as np
import torch.optim as optim
import torchvision.transforms as transforms
import torchvision.datasets as datasets
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

import time
import math

import pickle

AE_PATH = 'autoencoder.pt'
CHECKPOINT_PATH = 'checkpoint.pt'
CNN_PATH = "cnn.pt"
CNN_STATE = 'cnn_state.pt'

def imshow(img):
    img = img / 2 * 0.5
    npimg = img.numpy()
    plt.imshow(np.transpose(npimg, (1,2,0)))
    plt.show()

def as_minutes(s):

    m = math.floor(s/60)
    s -= m * 60

    return '%dm %ds' % (m, s)

def time_since(since, percent):

    now = time.time()

    s = now - since
    es = s / (percent)
    rs = es - s

    return '%s(-%s)' % (as_minutes(s), as_minutes(rs))

def serialize(autoencoder = None, cnn = None):
    if autoencoder is not None: 
        print("Serializing autoencoder...")
        try:
            torch.save(autoencoder.state_dict(), AE_PATH)
            print("Autoencoder serialized!")
            # output = open('autoencoder.pkl', 'wb')    
            # pickle.dump(autoencoder, output)
        except Exception: 
            print("Serialization fail.")

    if cnn is not None:
        print("Serializing Convolutional Neural Network...")
        try:
            torch.save(cnn.state_dict(), CNN_PATH)
            print("Convolution Neural Network serialized!")
            # output = open('autoencoder.pkl', 'wb')    
            # pickle.dump(autoencoder, output)
            torch.save({
            'model_state_dict': cnn.state_dict(),
            'optimizer_state_dict': cnn_optimizer.state_dict()
            }, CNN_STATE)

        except Exception: 
            print("Serialization fail.")

def deserialize(autoencoder = None, cnn = None, device = None):
    if autoencoder is not None: 
        try:
            print("Deserializing autoencoder...")
            # pkl_file = open('autoencoder.pkl', 'rb')    
            # autoencoder = pickle.load(pkl_file)
            autoencoder = Autoencoder(input_shape = 50176).to(device)
            autoencoder.load_state_dict(torch.load(AE_PATH, map_location = device))
            autoencoder.eval()

            # Adam optimizer

            optimizer = torch.optim.Adam(autoencoder.parameters(), lr = 1e-3)

            # mean-squared error loss
            criterion = nn.MSELoss()

            checkpoint = torch.load(CHECKPOINT_PATH)
            get_loss = checkpoint['loss']
            print("Loss from previous train: ", get_loss)
            print("Deserialized!")
        except Exception:
            print("Deserialization fail.")

    if cnn is not None:
        try:
            print("Deserializing Convolutional Neural Network...")
            
            cnn = ConvolutionalNetwork()
            cnn.load_state_dict(torch.load(CNN_PATH, map_location = device))
            cnn.to(device = device)

            cnn.eval()
            cnn_state = torch.load(CNN_STATE)
            # CNN criterion and Optimizer
            cnn_criterion = nn.CrossEntropyLoss()
            cnn_optimizer.load_state_dict(cnn_state['optimizer_state_dict'])
            # print("Optimizer's state_dict:")
            # for var_name in cnn_optimizer.state_dict():
            #     print(var_name, "\t", cnn_optimizer.state_dict()[var_name])
            
            print("Convolutional Neural Network deserialized!")
        except Exception:
            print("Deserialization fail.")
        


print("Cuda?:", torch.cuda.is_available())
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = Autoencoder(input_shape = 50176).to(device)

# Adam optimizer

optimizer = torch.optim.Adam(model.parameters(), lr = 1e-3)

# mean-squared error loss
criterion = nn.MSELoss()





# Potrebne putanje za Dataset klasu
annotation_train_path = 'data/TsignRecgTrain4170Annotation.txt'
annotation_test_path = 'data/TsignRecgTest1994Annotation.txt'
train_path = 'data/Traffic signs train'
test_path = 'data/Traffic signs test'

# Transformacija koja ce se koristiti u Dataset klasi
transform = transforms.Compose([transforms.Resize(255),
                                transforms.CenterCrop(224),
                                transforms.ToTensor()])

train_dataset = TrafficSign(train_path, annotation_train_path, transform = transform)

train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True, num_workers=0, pin_memory=True)

test_dataset = TrafficSign(test_path, annotation_test_path, transform=transform)
test_loader = DataLoader(test_dataset , batch_size=32, shuffle=False, num_workers=0)



train = input("Do you want to train the autoencoder?(y/n): ")

if train == 'y' or train == "Y":
    
    # Autoencoder train
    epochs = 1
    out = None
    counter = 0
    final_loss = 0
    start = time.time()
    for epoch in range(1, epochs+1):
        loss = 0
        # TODO: fix print
        print('%s %.4f' % (time_since(start, epoch / epochs), loss))
        for i, batch_features in enumerate(train_loader):
            print("{}/{}".format(i, len(train_loader)))
            images, labels = batch_features
            # reshape to [N, 784]
            #print(batch_features)
            images = images.view(-1, 50176).to(device)

            # reset the gradients back to zero
            optimizer.zero_grad()

            # compute reconstruction
            outputs = model(images)
            out = outputs.detach()
            counter += 1

            
            
            # compute training reconstruction loss
            train_loss = criterion(outputs, images)

            # compute accumulated gradients
            train_loss.backward()

            # perform parameter update based on current gradients
            optimizer.step()

            # add batch training loss to epoch loss
            loss += train_loss.item()

        print(epoch)
        # if epoch == 49:
        #     imshow(torchvision.utils.make_grid(out))
        loss = loss / len(train_loader)
        final_loss = loss
        # display the epoch training loss
        print("epoch : {}/{}, loss = {:.6f}".format(epoch + 1, epochs, loss))
        print('%s %.4f' % (time_since(start, epoch / epochs), loss))

    torch.save({
        "loss": final_loss
    }, CHECKPOINT_PATH)
else:
    
    deserialize(model, device = device)

test_examples = None


with torch.no_grad():
    for i, batch_features in enumerate(test_loader):
        images, labels = batch_features
        #imshow(torchvision.utils.make_grid(images))
        #images = images.view(-1, 784).to(device)
        #batch_features = batch_features[0]
        test_examples = images.view(-1, 50176).to(device)
        reconstruction = model(test_examples)
        break

with torch.no_grad():
    number = 10
    plt.figure(figsize=(20, 4))
    for index in range(number):
        # display original
        ax = plt.subplot(2, number, index + 1)
        plt.imshow(test_examples[index].cpu().numpy().reshape(224, 224))
        
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # display reconstruction
        ax = plt.subplot(2, number, index + 1 + number)
        
        plt.imshow(reconstruction[index].cpu().numpy().reshape(224, 224))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.show()

serialize(model)
print("Autoencoder finished.")




# Convolutional Neural Network
cnn = ConvolutionalNetwork().to(device = device)

# CNN criterion and Optimizer
cnn_criterion = nn.CrossEntropyLoss()
cnn_optimizer = optim.SGD(cnn.parameters(), lr = 1e-3, momentum = 0.9)
print("Optimizer's state_dict:")
for var_name in cnn_optimizer.state_dict():
    print(var_name, "\t", cnn_optimizer.state_dict()[var_name])


# # CNN training
train_cnn = input("Do you want to train the CNN?(y/n): ")

if train_cnn == 'y' or train_cnn == "Y":
    num_epochs = 2
    # lr = 0.001 u optimizatoru
    total_steps = len(train_loader)
    for epoch in range(num_epochs):
        running_loss = 0.0
        for i, data in enumerate(train_loader):
            images, labels = data
            
            # Forward pass
            outputs = cnn(images)
            loss = cnn_criterion(outputs, labels)
            
            # Backward and optimize
            cnn_optimizer.zero_grad()
            loss.backward()
            cnn_optimizer.step()
            running_loss  += loss.item()
            if i % 5 == 4:
                #print("Loss : {:.3f}".format(running_loss/10))
                print("Epoch [{} / {}], Step [{} / {}], Loss: {:.3f}".format(epoch+1, num_epochs, i+1, total_steps, running_loss/10))
                running_loss = 0.0
    
    print("Finished Training")
    serialize(cnn = cnn)

else:
    deserialize(cnn = cnn, device = device)



# Calculating CNN accuracy
total = 0
correct = 0

with torch.no_grad():
    for i, data in enumerate(test_loader):
        images, labels = data
        outputs = cnn(images)
        _, predict = torch.max(outputs.data, 1)
        total += labels.size(0)
        correct += (predict == labels).sum().item()

print("Accuracy: {:.3f}%".format(100 * correct / total))


# Predicting labels
with torch.no_grad():
    for i, data in enumerate(test_loader):
        images, labels = data
        
        outputs = cnn(images)
        _, predict = torch.max(outputs.data, 1)
        print(predict)
        imshow(torchvision.utils.make_grid(images))
        break









   
