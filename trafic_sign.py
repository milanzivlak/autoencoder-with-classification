import pandas as pd
import os
from natsort import natsorted
from torch.utils.data import Dataset
import PIL
class TrafficSign(Dataset):
    def __init__(self, main_dir, annotation_path, transform):
        self.annotation_path = annotation_path
        self.main_dir = main_dir
        self.transform = transform
        all_imgs = os.listdir(main_dir)
        self.total_imgs = natsorted(all_imgs)
        
    
    # Funkcija koja kategorije razvrstava u 13 posebnih klasa.
    # Svako ogranicenje brzine ima svoju zasebnu klasu(8 klasa), ostali su rasporedjeni u prohibitory, mandatory, warning i stop and yield.
    # Dodata je klasa None, jer nije postojalo kategorija 9,18,19,33 i mreza ima error bez toga.
    
    def populate_classes(self, category):
        if category == 0:
            return 1 # speed limit 5
        elif category == 1:
            return 2 # speed limit 15
        elif category == 2:
            return 3 # speed limit 30
        elif category == 3:
            return 4 # speed limit 40
        elif category == 4:
            return 5 # speec limit 650
        elif category == 5:
            return 6 # speed limit 60
        elif category == 6:
            return 7 # speed limit 70
        elif category == 7:
            return 8 # speed limit 80
        elif category in [9, 18, 19, 33]:
            return 0 # none
        elif category in range (9, 18) or category == 8:
            return 9 # prohibitory
        elif category in range(19, 32):
            return 10 # mandatory
        elif category in range(31, 52):
            return 11 # warning
        else:
            return 12 # stop and yield
        
    def __len__(self):
        return len(self.total_imgs)
    
    # Metoda __getitem__ ce vratiti par (image, label), odnosno sliku znaka i njegovu kategoriju.
    # U metodi je neophodno proci kroz ucitane podatke, dobaviti kategoriju i dobiti klasu preko prethodno definisane metode populate_classes().
    def __getitem__(self, idx):
        img_loc = os.path.join(self.main_dir, self.total_imgs[idx])
        image = PIL.Image.open(img_loc).convert("RGB")
        tensor_image = self.transform(image)
        
        category = None
        with open(self.annotation_path) as fp:
            file_data = pd.read_csv(fp, header = None, sep = ";")
            for row in file_data.index:
                if file_data[0][row] == self.total_imgs[idx]:
                    category = file_data[7][row]
                    category = self.populate_classes(category)
                else:
                    continue
            return (tensor_image, category) 