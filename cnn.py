import torch
import torchvision
from torch import nn
import torch.nn.functional as F

class ConvolutionalNetwork(nn.Module):
    def __init__(self):
        super(ConvolutionalNetwork, self).__init__()
        # ulazni_sloj: 3 ulazna kanala(3 color channels), 6 izlaznih kanala, velicina kernela 5
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2) # 2x2 matrica
        # ulazni sloj: izlaz od prethodnog 
        self.conv2 = nn.Conv2d(6, 16, 5)
        
        # Posljednja tri povezana sloja
        # U prethodnom ispisu, nakon zadnjeg pool-a sam dobio [32,16,53,53]
        self.fc1 = nn.Linear(16*53*53, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 13) # izlaz predstavlja broj klasa(kategorija)
        
    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        # -1 predstavlja broj primjera u batch-u
        x = x.view(-1, 16*53*53)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        #x = F.softmax(self.fc3(x), dim = 1)
        x = self.fc3(x)
        return x